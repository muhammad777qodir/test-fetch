package com.example.fetch.service;

import com.example.fetch.entity.University;
import com.example.fetch.payload.Status;
import com.example.fetch.repository.UniversityRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class UniversityService {

    @Autowired
    UniversityRepository universityRepository;

    public Status getById(long id) {
        Optional<University> university = universityRepository.findById(id);
        return university.isPresent() ?
                new Status(200, "success", university) :
                new Status(500, "error", null);
    }
}

package com.example.fetch.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@Entity
public class Region extends AbsEntity implements Serializable {

    private String name;

    @Column(name = "lan_region")
    private Double lanRegion;

    @Column(name = "lat_region")
    private Double latRegion;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "district_id", referencedColumnName = "district_id")
    private List<District> districts;

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}

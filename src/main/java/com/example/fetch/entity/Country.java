package com.example.fetch.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Getter
@Setter
@Entity
public class Country extends AbsEntity implements Serializable {

    private String name;

    @Column(name = "lan_country")
    private Double lanCountry;

    @Column(name = "lat_country")
    private Double latCountry;

    @OneToMany(fetch = FetchType.LAZY)
    @JoinColumn(name = "region_id", referencedColumnName = "region_id")
    private List<Region> regions;

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}

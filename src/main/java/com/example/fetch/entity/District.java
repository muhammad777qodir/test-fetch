package com.example.fetch.entity;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import java.util.List;

@Getter
@Setter
@ToString
@RequiredArgsConstructor
@Entity
public class District extends AbsEntity {

    private String name;

    @Column(name = "lan_district")
    private Double lanDistrict;

    @Column(name = "lat_district")
    private Double latDistrict;

    @OneToMany(fetch = FetchType.LAZY)
    @ToString.Exclude
    private List<Province> provinces;

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}

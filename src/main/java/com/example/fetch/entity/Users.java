package com.example.fetch.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.ManyToOne;

@Getter
@Setter
@Entity
public class Users extends AbsEntity {

    private String name;

    private String number;

    private String password;

    @ManyToOne(fetch = FetchType.LAZY)
    private Province province;

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}

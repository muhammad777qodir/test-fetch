package com.example.fetch.entity;

import lombok.Getter;
import lombok.Setter;

import javax.persistence.Column;
import javax.persistence.Entity;

@Getter
@Setter
@Entity
public class Province extends AbsEntity {

    private String name;

    @Column(name = "lan_province")
    private Double lanProvince;

    @Column(name = "lat_province")
    private Double latProvince;

    @Override
    public boolean equals(Object o) {
        return super.equals(o);
    }

    @Override
    public int hashCode() {
        return super.hashCode();
    }
}

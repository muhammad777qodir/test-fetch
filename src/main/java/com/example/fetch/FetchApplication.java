package com.example.fetch;

import com.example.fetch.entity.*;
import com.example.fetch.repository.*;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;

import java.util.List;

@SpringBootApplication
public class FetchApplication {

    public static void main(String[] args) {
        SpringApplication.run(FetchApplication.class, args);
    }

    @Bean
    public CommandLineRunner commandLineRunner(
            ProvinceRepository provinceRepository,
            DistrictRepository districtRepository,
            RegionRepository regionRepository,
            CountryRepository countryRepository,
            UsersRepository usersRepository,
            UniversityRepository universityRepository
    ) {
        return args -> {

            Province province = new Province();
            province.setName("s.ayni");
            province.setLanProvince(41.545454545454);
            province.setLatProvince(69.5454545454545);

            Province province2 = new Province();
            province2.setName("s.ayni2");
            province2.setLanProvince(41.545454545454);
            province2.setLatProvince(69.5454545454545);
            List<Province> savedProvince = provinceRepository.saveAll(List.of(province, province2));


            District district = new District();
            district.setName("navoiy shaxar");
            district.setLanDistrict(41.22222222222);
            district.setLatDistrict(69.45454545454);
            district.setProvinces(List.of(savedProvince.get(0)));

            District district2 = new District();
            district2.setName("navoiy shaxar2");
            district2.setLanDistrict(41.22222222222);
            district2.setLatDistrict(69.45454545454);
            district2.setProvinces(List.of(savedProvince.get(1)));
            List<District> savedDistrict = districtRepository.saveAll(List.of(district, district2));

            Region region = new Region();
            region.setName("navoiy");
            region.setLanRegion(41.000000000);
            region.setLatRegion(69.555555555);
            region.setDistricts(List.of(savedDistrict.get(0)));

            Region region2 = new Region();
            region2.setName("navoiy2");
            region2.setLanRegion(41.000000000);
            region2.setLatRegion(69.555555555);
            region2.setDistricts(List.of(savedDistrict.get(1)));
            List<Region> savedRegion = regionRepository.saveAll(List.of(region, region2));

            Country country = new Country();
            country.setName("uzb");
            country.setLanCountry(41.222222222222);
            country.setLatCountry(69.545454545454);
            country.setRegions(List.of(savedRegion.get(0)));

            Country country2 = new Country();
            country2.setName("uzb2");
            country2.setLanCountry(41.222222222222);
            country2.setLatCountry(69.545454545454);
            country2.setRegions(List.of(savedRegion.get(1)));
            countryRepository.saveAll(List.of(country, country2));


            Users users = new Users();
            users.setName("Muhammad");
            users.setNumber("+998997226841");
            users.setPassword("123");
            users.setProvince(savedProvince.get(0));

            Users users2 = new Users();
            users2.setName("Ali");
            users2.setNumber("+998997226842");
            users2.setPassword("123");
            users2.setProvince(savedProvince.get(1));
            List<Users> savedUser = usersRepository.saveAll(List.of(users, users2));

            University university = new University();
            university.setName("tatu");
            university.setArchives("kncjksdbvcbsjvhbsjbvjsdhbjhbdvsjvhsjdhbvjhsdbvjhsdbjhvbsdjh");
            university.setRank(7555L);
            university.setUsers(savedUser);
            university.setCountries(List.of(country, country2));
            universityRepository.save(university);

        };
    }

}
